SharePoint Holiday Loader 

Information: https://bitbucket.org/rmaclean/sharepoint-holiday-loader

SharePoint Holiday Loader allows you to quickly import public holidays into a SharePoint calendar from the standard .HOL format.

Version: 2.0.0.0