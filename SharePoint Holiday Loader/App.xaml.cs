﻿
namespace SADev.SharePointHolidayLoader
{
    using System;
using System.Windows;
using AtomicMVVM;
using SADev.SharePointHolidayLoader.ViewModels;
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static Bootstrapper<MainWindow, SimpleLoading> Bootstrapper;

        public App()
        {
            Bootstrapper = new Bootstrapper<MainWindow, SimpleLoading>();
        }
    }

    //class Startup
    //{
    //    [STAThread]
    //    static void Main()
    //    {
    //        App app = new App() { MainWindow = new MainWindow() };
    //        app.MainWindow.Show();
    //        app.Run();
    //    }

    //}

}
