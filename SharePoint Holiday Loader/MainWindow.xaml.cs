﻿namespace SADev.SharePointHolidayLoader
{
    using System.Linq;
    using System.Windows.Controls;
    using AtomicMVVM;
    using MahApps.Metro;
    using MahApps.Metro.Controls;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow, IShell
    {
        public MainWindow()
        {
            InitializeComponent();
            ThemeManager.ChangeTheme(this, ThemeManager.DefaultAccents.Single(_ => _.Name == "Blue"), Theme.Light);
        }             
    
        public void ChangeContent(UserControl viewContent)
        {
            ContentPanel.Content = viewContent;
        }
    }
}
