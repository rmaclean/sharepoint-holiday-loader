﻿namespace SADev.SharePointHolidayLoader.Models
{
    using System;
    using System.Collections.Generic;

    public class Country
    {
        private readonly List<Holiday> holidays = new List<Holiday>();
        public string Name { get; private set; }
        public int ID { get; private set; }
        public List<Holiday> Holidays
        {
            get
            {
                return holidays;
            }
        }

        public Country(string name, int id)
        {
            this.Name = name;
            this.ID = id;
        }        

        internal void AddHoliday(string name, DateTime date)
        {
            holidays.Add(new Holiday(name, date));
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
}
