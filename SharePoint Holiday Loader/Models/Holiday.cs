﻿namespace SADev.SharePointHolidayLoader.Models
{
    using System;

    public class Holiday
    {
        public Holiday(string name, DateTime date)
        {
            this.Event = name;
            this.Date = date;
        }

        public string Event { get; set; }
        public DateTime Date { get; set; }
    }
}
