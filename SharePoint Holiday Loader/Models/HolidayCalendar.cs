﻿namespace SADev.SharePointHolidayLoader.Models
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text.RegularExpressions;
    using System.Xml.Linq;
    using SADev.SharePointHolidayLoader.Models;
    using SharePointListService.SADev.SharePoint;

    public class HolidayCalendar
    {
        private static XNamespace SoapNamespace = "http://schemas.microsoft.com/sharepoint/soap/";             

        public ObservableCollection<Country> Countries { get; set; }
        public ObservableCollection<string> Calendars { get; set; }

        public HolidayCalendar()
        {
            Countries = new ObservableCollection<Country>();
            Calendars = new ObservableCollection<string>();
        }

        public Exception LastException { get; private set; }

        public void LoadCalendars(Uri sharePointUri, NetworkCredential credential)
        {
            Calendars.Clear();
            using (var list = SharePointList(sharePointUri, credential))
            {
                try
                {
                    var result = list.GetListCollection();
                    var document = XDocument.Parse(result.OuterXml);
                    var calendarNodes = from _ in document.Descendants(SoapNamespace+"List")
                                        where _.Attribute("FeatureId").Value == "00bfea71-ec85-4903-972d-ebe475780106"
                                        let name = _.Attribute("Title").Value
                                        orderby name
                                        select name;

                    foreach (var item in calendarNodes)
                    {
                        Calendars.Add(item);
                    }
                }
                catch (WebException)
                {
                    //do nothing
                }
            }
        }

        public void Parse(string fileName)
        {
            Countries.Clear();
            var counter = 0;
            var countryParserRegEx = new Regex(@"\[(?<Country>.+)\]\s(?<ID>\d+)");
            var eventParserRegEx = new Regex(@"(?<Event>[\w\s\p{P}^,]+)\s*,\s*(?<Date>\d{4}/\d{1,2}/\d{1,2})");
            var minDate = new DateTime(1900, 1, 1);
            var maxDate = new DateTime(8900, 12, 31);

            var lines = from l in File.ReadAllLines(fileName)
                        where !string.IsNullOrWhiteSpace(l)
                        select l.Trim();

            Country country = null;

            foreach (var line in lines)
            {
                if (line.StartsWith("["))
                {
                    var match = countryParserRegEx.Match(line);
                    if (!match.Success)
                    {
                        continue;
                    }

                    if (country != null)
                    {
                        Countries.Add(country);
                    }

                    var name = match.Groups["Country"].Value;
                    var id = Convert.ToInt32(match.Groups["ID"].Value);

                    country = new Country(name, id);
                    continue;
                }
                else
                {
                    if (country == null)
                    {
                        continue;
                    }

                    var match = eventParserRegEx.Match(line);
                    if (!match.Success)
                    {
                        continue;
                    }

                    var name = match.Groups["Event"].Value;
                    DateTime date;
                    try
                    {
                        date = DateTime.ParseExact(match.Groups["Date"].Value, "yyyy/M/d", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal | DateTimeStyles.AllowWhiteSpaces);
                    }
                    catch (FormatException err)
                    {
                        if (err.Message.Equals("The DateTime represented by the string is not supported in calendar System.Globalization.GregorianCalendar."))
                        {
                            continue;
                        }

                        throw;
                    }

                    if (date >= minDate && date <= maxDate)
                    {
                        country.AddHoliday(name, date);
                    }

                    counter++;
                }
            }

            if (country != null && country.Holidays.Count() > 0)
            {
                Countries.Add(country);
            }

            if (Countries.Count == 0)
            {
                throw new Exception("No countries");
            }
        }

        private Lists SharePointList(Uri sharePointUri, NetworkCredential credential)
        {
            var sharePointList = new SharePointListService.SADev.SharePoint.Lists();
            sharePointList.Url = string.Format(CultureInfo.CurrentCulture, "{0}/_vti_bin/lists.asmx", sharePointUri);
            if (credential != null)
            {
                sharePointList.Credentials = credential;
            }
            else
            {
                sharePointList.UseDefaultCredentials = true;
            }

            return sharePointList;
        }

        public bool Publish(Uri sharePointUri, string calendar, NetworkCredential credential, Country country)
        {
            using (var sharePointList = SharePointList(sharePointUri, credential))
            {
                try
                {
                    var listViewNode = sharePointList.GetListAndView(calendar, string.Empty);

                    var listId = listViewNode.ChildNodes[0].Attributes["Name"].Value;
                    var viewId = listViewNode.ChildNodes[1].Attributes["Name"].Value;
                    var batch = SharePointHelpers.CreateBatch(viewId);
                    foreach (var holiday in country.Holidays)
                    {
                        var fields = new Dictionary<string, object>();
                        fields.Add("Title", holiday.Event);
                        fields.Add("Location", country);
                        fields.Add("fAllDayEvent", true);
                        fields.Add("EventDate", holiday.Date.ToSharePointDateTimeFormat());
                        fields.Add("EndDate", holiday.Date.ToSharePointDateTimeFormat());
                        fields.Add("Category", "Holiday");
                        batch.AddNewItemsToBatch(fields);
                    }

                    batch.Insert(sharePointList, listId);
                    return true;
                }
                catch (Exception err)
                {
                    this.LastException = err;
                    return false;
                }
            }
        }
    }
}
