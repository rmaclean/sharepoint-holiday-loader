﻿
namespace SADev.SharePointHolidayLoader.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Windows.Forms;
    using AtomicMVVM;
    using SADev.SharePointHolidayLoader.Models;
    using SADev.SharePointHolidayLoader.Properties;

    public class SimpleLoading : CoreData
    {
        private string filename;
        private string sharepointUrl;
        private string username;
        private string password;
        private string domain;
        private Uri sharepointUri;
        private bool inProgress;
        private HolidayCalendar holidayCalendar;
        private Country selectedCountry;
        private string selectedCalendar;

        public string SelectedCalendar
        {
            get { return selectedCalendar; }
            set
            {
                selectedCalendar = value;
                RaisePropertyChanged("SelectedCalendar");
            }
        }

        public Country SelectedCountry
        {
            get { return selectedCountry; }
            set
            {
                selectedCountry = value;
                RaisePropertyChanged("SelectedCountry");
            }
        }

        public ObservableCollection<string> Messages { get; set; }

        public HolidayCalendar HolidayCalendar
        {
            get { return holidayCalendar; }
            set
            {
                holidayCalendar = value;
                RaisePropertyChanged("HolidayCalendar");
            }
        }

        public bool InProgress
        {
            get { return inProgress; }
            set
            {
                inProgress = value;
                RaisePropertyChanged("InProgress");
            }
        }

        public string Domain
        {
            get { return domain; }
            set
            {
                if (domain != value)
                {
                    domain = value;
                    RaisePropertyChanged("Domain");
                    Settings.Default.PreviousDomain = value;
                    Settings.Default.Save();
                }
            }
        }

        public string Password
        {
            get { return password; }
            set
            {
                password = value;
                RaisePropertyChanged("Password");
            }
        }

        public string Username
        {
            get { return username; }
            set
            {
                if (username != value)
                {
                    username = value;
                    RaisePropertyChanged("Username");
                    Settings.Default.PreviousUsername = value;
                    Settings.Default.Save();
                }
            }
        }

        public string SharePointUrl
        {
            get { return sharepointUrl; }
            set
            {
                if (sharepointUrl != value)
                {
                    sharepointUrl = value;
                    RaisePropertyChanged("SharePointUrl");
                    Settings.Default.PreviousURL = value;
                    Settings.Default.Save();
                    RefreshCalendars();
                }
            }
        }

        public string Filename
        {
            get { return filename; }
            set
            {
                if (filename != value)
                {
                    filename = value;
                    RaisePropertyChanged("Filename");
                    ParseFile();
                    Settings.Default.PreviousPath = value;
                    Settings.Default.Save();
                }
            }
        }

        private void ParseFile()
        {
            if (File.Exists(this.Filename))
            {
                try
                {
                    holidayCalendar.Parse(this.Filename);
                }
                catch (Exception err)
                {
                    Messages.Insert(0, string.Format(CultureInfo.CurrentCulture, "An error occurred trying to publish calendar entries.{0}{1}", Environment.NewLine, err.Message));
                    return;
                }
            }
        }

        public SimpleLoading()
        {
            Messages = new ObservableCollection<string>();
            HolidayCalendar = new HolidayCalendar();
            this.OnBound += (s, e) =>
                {
                    this.SharePointUrl = Settings.Default.PreviousURL;
                    this.Username = Settings.Default.PreviousUsername;
                    this.Domain = Settings.Default.PreviousDomain;
                };
        }

        public void SelectFile()
        {
            var initialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            if (!string.IsNullOrWhiteSpace(Settings.Default.PreviousPath))
            {
                initialDirectory = Path.GetFullPath(Settings.Default.PreviousPath);
            }

            var selectHolidayFileDialog = new OpenFileDialog
            {
                CheckFileExists = true,
                CheckPathExists = true,
                DefaultExt = "hol",
                Filter = "Holiday Files (*.hol)|*.hol|All Files (*.*)|*.*",
                InitialDirectory = initialDirectory,
                Multiselect = false,
                ShowReadOnly = true
            };

            selectHolidayFileDialog.FileOk += (sender, e) =>
                {
                    this.Filename = selectHolidayFileDialog.FileName;
                };

            selectHolidayFileDialog.ShowDialog();
        }

        [ReevaluateProperty("Filename", "SharePointUrl", "CalendarName",
                            "Username", "Password", "Domain", "SelectedCountry")]
        public bool CanImportHolidays()
        {
            var result = !string.IsNullOrWhiteSpace(this.Filename) &&
                    File.Exists(this.Filename) &&
                    !string.IsNullOrWhiteSpace(SharePointUrl) &&
                    Uri.TryCreate(SharePointUrl, UriKind.Absolute, out sharepointUri) &&
                    this.selectedCalendar != null &&
                    this.SelectedCountry != null;

            if (!result)
            {
                return false;
            }

            if (!string.IsNullOrWhiteSpace(Username) || !string.IsNullOrWhiteSpace(Password) || !string.IsNullOrWhiteSpace(Domain))
            {
                result = !string.IsNullOrWhiteSpace(Username) && !string.IsNullOrWhiteSpace(Password);
            }

            return result;
        }

        private NetworkCredential GetCredentials()
        {
            NetworkCredential credential = null;
            if (!string.IsNullOrWhiteSpace(this.Username) ||
                !string.IsNullOrWhiteSpace(this.Password) ||
                !string.IsNullOrWhiteSpace(this.Domain))
            {
                if (!string.IsNullOrWhiteSpace(this.Domain))
                {
                    credential = new NetworkCredential(this.Username, this.Password, this.Domain);
                }
                else
                {
                    credential = new NetworkCredential(this.Username, this.Password);
                }
            }

            return credential;
        }

        public void ImportHolidays()
        {
            var credential = GetCredentials();

            InProgress = true;
            bool successful = holidayCalendar.Publish(this.sharepointUri, this.SelectedCalendar, credential, SelectedCountry);

            if (!successful)
            {
                Messages.Insert(0, string.Format(CultureInfo.CurrentCulture, "An error occurred trying to publish calendar entries.{0}{1}", Environment.NewLine, holidayCalendar.LastException.Message));
                return;
            }

            Messages.Insert(0, "Successfully completed import");
            InProgress = false;
        }

        [ReevaluateProperty("SharePointUrl", "Username", "Password", "Domain")]
        public bool CanRefreshCalendars()
        {
            var result = !string.IsNullOrWhiteSpace(SharePointUrl) &&
                    Uri.TryCreate(SharePointUrl, UriKind.Absolute, out sharepointUri);

            if (!result)
            {
                return false;
            }

            if (!string.IsNullOrWhiteSpace(Username) || !string.IsNullOrWhiteSpace(Password) || !string.IsNullOrWhiteSpace(Domain))
            {
                result = !string.IsNullOrWhiteSpace(Username) && !string.IsNullOrWhiteSpace(Password);
            }

            return result;
        }

        public void RefreshCalendars()
        {
            if (CanRefreshCalendars() && Uri.TryCreate(SharePointUrl, UriKind.Absolute, out sharepointUri))
            {
                holidayCalendar.LoadCalendars(this.sharepointUri, this.GetCredentials());
            }
        }

        public void GetMoreInfo()
        {
            System.Diagnostics.Process.Start("https://bitbucket.org/rmaclean/sharepoint-holiday-loader/");
        }
    }
}
